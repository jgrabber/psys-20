#ifndef _CORE_H
#define _CORE_H

typedef enum {
	CORE_OK = 0,
	CORE_INVALID_ARGS,
	CORE_INTERNAL_ERROR,
} CORE_ERROR;

CORE_ERROR error2string(CORE_ERROR error, char** error_string);

CORE_ERROR integrate(double (*f)(double x), double l, double r, int steps, double* integral);

CORE_ERROR numeric_pi(int num_samples, double* pi);

#endif
