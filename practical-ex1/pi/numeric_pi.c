/**
 * @file numeric_pi.c
 *
 * This is the main driver of the program, i.e.,
 * the program, which is then used by the user.
 */
#include <stdio.h>
#include <stdlib.h>
#include "core.h"

int main(int argc, char **argv)
{
	int num_samples;

	if (argc < 2)
	{
		fprintf(stderr,"Not enough arguments given!\n");
		return EXIT_FAILURE;
	}

	/* Normally, we would check for wrong input formats (i.e., no numbers) */
	num_samples = atoi(argv[1]);

	double pi;
	CORE_ERROR error;
	if ((error = numeric_pi(num_samples, &pi) != CORE_OK))
	{
		char* error_string = "";
		error2string(error, &error_string);
		printf("Could not calculate PI: %s\n", error_string);
		return EXIT_FAILURE;
	}

	printf("PI: %.30f\n", pi);

	return EXIT_SUCCESS;
}
