/**
 * Implements the core of the algorithm.
 *
 * @file core.c
 */
#define DEBUG
#include "core.h"

#include <math.h>

#include <omp.h>

#ifdef DEBUG
#include "stdio.h"
#endif

double half_circle(double);

/** 
 * Provides the name of the given error.
 *
 * @param error to provide the name for
 * @param error_string to receive the name on
 * @return error status code
 */
CORE_ERROR error2string(CORE_ERROR error, char** error_string)
{
	switch (error)
	{
		case CORE_OK: 
			*error_string = "OK";
			return CORE_OK;
		case CORE_INVALID_ARGS:
			*error_string = "INVALID ARGUMENTS";
			return CORE_OK;
		case CORE_INTERNAL_ERROR:
			*error_string = "INTERNAL ERROR";
			return CORE_OK;
		default:
			return CORE_INVALID_ARGS;
	}
}

/**
 * Integrates the given function between two given points
 * in a given amount of steps.
 *
 * @param f function to integrate
 * @param l left boundary of range to integrate over
 * @param r right boundary of range to integrate over
 * @param steps number of steps to take/sampling rate
 * @param integral pointer which will hold the result
 * @return error status code
 */
CORE_ERROR integrate(double (*f)(double x), double l, double r, int steps, double* integral)
{
	if (r <= l || steps < 1)
	{
		return CORE_INVALID_ARGS;
	}

	double stepwidth = (r - l) / (double)steps;
	double start_x = l + stepwidth / 2.0;

	double local_integral = 0.0;

//#pragma omp parallel shared(f, steps, stepwidth, start_x, local_integral) default(none)
//	{
//		double thread_integral = 0.0;
//
//#pragma omp for
//		for (int step = 0; step < steps; step++)
//		{
//			thread_integral += f(start_x + step * stepwidth) * stepwidth;
//
//		}
//#pragma omp critical
//		{
//			local_integral += thread_integral;
//		}
//	}
#pragma omp parallel for shared(f, steps, stepwidth, start_x) default(none) reduction(+: local_integral)
	for (int step = 0; step < steps; step++)
	{
		local_integral += f(start_x + step * stepwidth) * stepwidth;

	}

	*integral = local_integral;
	return CORE_OK;
}

/**
 * Approximates PI with the given amount of samples.
 *
 * @param samples number of samples to take
 * @param pi pointer to numerically approximated value of PI
 * @return error status code
 */
CORE_ERROR numeric_pi(int num_samples, double* pi)
{
	if (num_samples < 1)
	{
		return CORE_INVALID_ARGS;
	}

	double integral;
	if (integrate(&half_circle, -1, 1, num_samples, &integral) != CORE_OK)
	{
		// Should never happen, but hide underlying error from user.
		return CORE_INTERNAL_ERROR;
	}
	*pi = integral * 2;
	return CORE_OK;
}

/**
 * Calculates the value of x between -1 and 1 on the half circle
 * from (-1,0) over (1,1) to (1,0).
 *
 * @param x point at which to calculate y
 */
double half_circle(double x)
{
	return sqrt(1-x*x);
}
