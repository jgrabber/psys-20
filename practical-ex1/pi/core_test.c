/**
 * Test for functions in core.c
 *
 * We don't need to test anything here really.
 */
#include <assert.h>
#include <math.h>
#include <stdio.h>

/* Include the module under testing */
#include "core.c"

/******************************************************/

static double constant_0(double ignored)
{
	return 0.0;
}

static double constant_2(double ignored)
{
	return 2.0;
}

static double linear_4(double x)
{
	return 4.0 * x;
}

static double squared(double x)
{
	return x * x;
}

static int within_epsilon(double a, double b, double epsilon)
{
	return fabs(a - b) < epsilon;
}

static void test__integrate_calculates_integral()
{
	double epsilon = 0.00000001;
	double integral = -1.0;

	assert(integrate(&constant_0, 0, 1, 1, &integral) == CORE_OK
			&& integral == 0.0);
	assert(integrate(&constant_0, -100, -10, 312, &integral) == CORE_OK
			&& integral == 0.0);
	assert(integrate(&constant_2, 0, 10, 1, &integral) == CORE_OK
			&& integral == 20.0);

	assert(integrate(&constant_2, -3, -2.5, 1000, &integral) == CORE_OK
	      && within_epsilon(integral, 1.0, epsilon));
	assert(integrate(&constant_2, -3, -2.5, 1000, &integral) == CORE_OK
	      && within_epsilon(integral, 1.0, epsilon));
	assert(integrate(&linear_4, 10, 11, 33, &integral) == CORE_OK
			&& within_epsilon(integral, 42.0, epsilon));
	assert(integrate(&squared, -100, -50, 2, &integral) == CORE_OK
			&& within_epsilon(integral, -87.5*-87.5 * 25 + -62.5*-62.5 * 25, epsilon));

	integral = 99;
	assert(integrate(&linear_4, 10, 0, 1, &integral) == CORE_INVALID_ARGS);
	assert(integrate(&linear_4, 0, 10, 0, &integral) == CORE_INVALID_ARGS);
	assert(integrate(&linear_4, 0, 10, -1, &integral) == CORE_INVALID_ARGS);
	assert(integral == 99);
}

static void test__numeric_pi_approximates_pi()
{
	double pi;
	assert(numeric_pi(0, &pi) == CORE_INVALID_ARGS);
	assert(numeric_pi(-1, &pi) == CORE_INVALID_ARGS);

	assert(numeric_pi(1, &pi) == CORE_OK
			&& pi == 4.0);
	assert(numeric_pi(10, &pi) == CORE_OK
			&& fabs(M_PI - pi) < .1);
	assert(numeric_pi(100, &pi) == CORE_OK
			&& fabs(M_PI - pi) < .01);
	assert(numeric_pi(1000000, &pi) == CORE_OK
			&& fabs(M_PI - pi) < .000001);
}

/**
 * Main entry for the test.
 */
int main(int argc, char **argv)
{
	test__integrate_calculates_integral();
	test__numeric_pi_approximates_pi();
	return 0;
}
